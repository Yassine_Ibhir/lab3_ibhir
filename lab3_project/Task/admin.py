from django.contrib import admin

from .models import TheTask,TheResponsible

admin.site.register(TheTask)
admin.site.register(TheResponsible)