from django.urls import path

from . import views,restfullApi_views as rav

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('contact/', views.ContactPageView.as_view(), name='contact'),
    path('task_list/',views.GetAllTasksList.as_view(),name='task-list'),
    path('<int:pk>/', views.GetTaskDetail.as_view(), name='task-detail'),
    path('responsible_list/',views.GetAllResponsibleList.as_view(),name='responsible-list'),

# restfullApi paths
    path('api/', rav.task_api_overview, name='api'),
    path('api/task_list/', rav.api_task_list, name='api-task-list'),
    path('api/<int:pk>/task_detail', rav.api_task_detail, name='api-task-detail'),
]
