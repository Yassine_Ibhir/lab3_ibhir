from rest_framework.serializers import ModelSerializer
from .models import TheTask


class TaskSerializer(ModelSerializer):
    class Meta:
        model = TheTask
        fields = '__all__'
