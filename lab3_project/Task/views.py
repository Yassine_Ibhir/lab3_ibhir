from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from .models import TheTask, TheResponsible


# render the home page
def home(request):
    context = {'title': 'index'}
    return render(request, 'Task/home.html', context)


# renders about page by inheriting from TemplateView
class AboutPageView(TemplateView):
    template_name = 'Task/about.html'

    # passing title to the template
    def get_context_data(self, *args, **kwargs):
        context = super(AboutPageView, self).get_context_data(*args, **kwargs)
        context['title'] = 'about'
        return context


# renders contact page by inheriting from TemplateView
class ContactPageView(TemplateView):
    template_name = 'Task/contact.html'

    # passing title to the template
    def get_context_data(self, *args, **kwargs):
        context = super(ContactPageView, self).get_context_data(*args, **kwargs)
        context['title'] = 'contacts'
        return context


# Yassine Ibhir
# Class inherits from ListView to display all tasks
class GetAllTasksList(ListView):
    model = TheTask
    template_name = 'Task/task_list.html'
    context_object_name = 'task_list'
    paginate_by = 8

    # passing title and task-list to the template
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'list tasks'
        return context


# Yassine Ibhir
# Class inherits from DetailView to display one Task
class GetTaskDetail(DetailView):
    model = TheTask
    template_name = 'Task/task_detail.html'

    # passing title and task-list to the template
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "task's details"
        return context


# Yassine Ibhir
# Class inherits from ListView to display all responsible
class GetAllResponsibleList(ListView):
    model = TheResponsible
    template_name = 'Task/responsible_list.html'
    context_object_name = 'responsible_list'
    paginate_by = 8

    # passing title and responsible-list to the template
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'list respnsibles'
        return context


